import React, {useContext} from 'react';
import './starwars.css';
import {Spinner, Alert, StarwarsUserItems} from '../../components';
import {StarwarsContext} from '../../context';

export const StarwarsTab = () => {
    const {planets, loading} = useContext(StarwarsContext);
    return (
        <div className="planet-table">
            {loading ? <Spinner/> : 
                (planets == null ?
                    <Alert /> 
                    :
                    planets.map((planet, i) => (
                    <div className="planets" key={i}>
                        <div className="card text-white bg-dark mb-4" key={i}>
                            <div className="card-header">{planet.name}</div>
                            <div className="card-body">
                                <h5 className="card-title">Diameter: {planet.diameter}</h5>
                                <p className="card-text">Population: {planet.population}<br /> 
                                Orbital period: {planet.orbital_period} <br />
                                Rotation period: {planet.rotation_period} <br />
                                Climate: {planet.climate}
                                </p>
                            </div>
                        </div>
                    </div>
                )))
            }
            <StarwarsUserItems />
        </div>
    );
}

// @TODO
// Handle null response

//useReducer context for fetch