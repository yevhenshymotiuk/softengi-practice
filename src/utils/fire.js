import firebase from 'firebase';

var firebaseConfig = {
    apiKey: "AIzaSyDb32_YbRbUX6ZQZc5rDOp4TR46kbdVPmo",
    authDomain: "api-testing-softengi.firebaseapp.com",
    databaseURL: "https://api-testing-softengi.firebaseio.com",
    projectId: "api-testing-softengi",
    storageBucket: "api-testing-softengi.appspot.com",
    messagingSenderId: "382940828862",
    appId: "1:382940828862:web:1575d90fc03f6e0d9b3aef"
};

const fire = firebase.initializeApp(firebaseConfig);
export default fire;