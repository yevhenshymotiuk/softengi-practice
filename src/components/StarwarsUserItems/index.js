// Handles > components
// Reducer > context
// planets with id
// sort by smth



import React, {useReducer, useRef, useEffect} from 'react';
import {Button} from '../'
import './starwarsUserItems.css'



export const StarwarsUserItems = () => {
    const inputName = useRef();
    const inputDiameter = useRef();
    const inputPopulation = useRef();
    const inputOrbital = useRef();
    const inputRotation = useRef();
    const inputClimate = useRef();
    const inputEditName = useRef();
    const inputEditDiameter = useRef();
    const inputEditPopulation = useRef();
    const inputEditOrbital = useRef();
    const inputEditRotation = useRef();
    const inputEditClimate = useRef();
    
    const initialState = {
        planets: []
    }

    const reducer = (state, action) => {
        switch(action.type) {
            case 'uploadPlanets': 
                return {
                    planets: action.planets
                }
            
            case 'add':
                return {
                    planets: action.planets
                }
                    
            case 'delete':
                return {
                    planets: action.planets
                }
            case 'edit':
                return {
                    planets: action.planets
                }
            case 'toggle':
                return {
                    planets: action.planets
                }    
            default:
                return 0;
        }
    };
    const [state, dispatch] = useReducer(reducer, initialState);
    // const [planetState, setPlanetState] = useState([])
    useEffect(() => {
        const storage = JSON.parse(localStorage.getItem("allPlanets")) || [];
        dispatch({
            type: 'uploadPlanets',
            planets: storage
        })

    }, [])
    
    
    const handleDelete = (id) => {
        // event.preventDefault();
        console.log(id)
        
        const planetsLeft = state.planets.filter(planet => planet.id !== id)
        dispatch({
            type: 'delete',
            planets: planetsLeft
        })
        console.log(state.planets)
        localStorage.setItem("allPlanets", JSON.stringify(planetsLeft))
    }

    const handleAdd = (event) => {
        event.preventDefault();
        state.planets.push(
            {
                id: Date.now(),
                name: inputName.current.value,
                diameter: inputDiameter.current.value,
                population: inputPopulation.current.value,
                orbital: inputOrbital.current.value,
                rotation: inputRotation.current.value,
                climate: inputClimate.current.value,
                editable: false
            })
        dispatch({
            type: 'add',
            planets: state.planets
        });
        inputName.current.value = '';
        inputDiameter.current.value = '';
        inputPopulation.current.value = '';
        inputOrbital.current.value = '';
        inputRotation.current.value = '';
        inputClimate.current.value = '';

        localStorage.setItem("allPlanets", JSON.stringify(state.planets))
    
    }

    const handleEdit = (id) => {
        state.planets.forEach(planet => {
            let bool = planet.editable !== true;
            if (planet.id !== id) {
                planet.editable = false;
            }
            if (planet.id === id) {
                planet.editable = bool;
            }
        });
        dispatch({
            type: 'toggle',
            planets: state.planets
        });

    }
    const handleSubmitEdit = (id) => {
        state.planets.forEach(planet => {
            if (planet.id === id) {
                planet.name = inputEditName.current.value;
                planet.diameter = inputEditDiameter.current.value;
                planet.population = inputEditPopulation.current.value;
                planet.orbital = inputEditOrbital.current.value;
                planet.rotation = inputEditRotation.current.value;
                planet.climate = inputEditClimate.current.value;
                planet.editable = false;
            }
        });
        dispatch({
            type: 'edit',
            planets: state.planets
        });
        inputEditName.current.value = '';
        inputEditDiameter.current.value = '';
        inputEditPopulation.current.value = '';
        inputEditOrbital.current.value = '';
        inputEditRotation.current.value = '';
        inputEditClimate.current.value = '';

        localStorage.setItem("allPlanets", JSON.stringify(state.planets))
    }
    
    return(
        <>
            {state.planets.map((planet) => (
                <div className="planets" key={planet.id}>
                    <div className="card text-white bg-dark mb-4">
                        <div className="card-header"> 
                            {planet.editable ? 
                            <input placeholder={planet.name} ref={inputEditName}/> 
                            : 
                            <span>{planet.name}</span>} 
                            <Button className="card-btn" onClick={() => handleDelete(planet.id)} text="X" />
                            <Button className="card-btn" onClick={() => handleEdit(planet.id)} text="E" />
                        </div>
                        <div className="card-body">
                            <h5 className="card-title">
                                Diameter: {planet.editable ? <input placeholder={planet.diameter}  ref={inputEditDiameter} /> : <span>{planet.diameter}</span> }
                            </h5>
                                <p className="card-text">
                                    Population: {planet.editable ? <input placeholder={planet.population}  ref={inputEditPopulation}/> : <span>{planet.population}</span> }<br /> 
                                    Orbital period: {planet.editable ? <input placeholder={planet.orbital}  ref={inputEditOrbital}/> : <span>{planet.orbital}</span> } <br />
                                    Rotation period: {planet.editable ? <input placeholder={planet.rotation}  ref={inputEditRotation}/> : <span>{planet.rotation}</span> } <br />
                                    Climate: {planet.editable ? <input placeholder={planet.climate}  ref={inputEditClimate}/> : <span>{planet.climate}</span> }
                                </p>
                                {planet.editable ? <Button text="Submit" onClick={() => handleSubmitEdit(planet.id)} /> : ''}
                        </div>
                    </div>
                </div>
                ))
            }
            <form className="planets" onSubmit={handleAdd}>

                <div className="card text-white bg-dark mb-4">
                    <div className="card-header">
                        <input placeholder="Name" ref={inputName} />
                    </div>
                    <div className="card-body">
                        <h5 className="card-title">
                            Diameter: <input placeholder="Size" ref={inputDiameter} type="text" className="bottom-inputs" required/>
                        </h5>
                        <p className="card-text">
                            Population: <input placeholder="Number" ref={inputPopulation} type="text" className="bottom-inputs" required/><br /> 
                            Orbital period:  <input placeholder="Cycle" ref={inputOrbital} type="text" className="bottom-inputs" required/><br />
                            Rotation period:  <input placeholder="Cycle" ref={inputRotation} type="text" className="bottom-inputs" required/><br />
                            Climate: 
                            <select ref={inputClimate} type="text" className="bottom-inputs" defaultValue="1" required>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                                <option value="4">Four</option>
                                <option value="5">Five</option>
                            </select>
                            <button type="submit" className="submit-button">Submit</button>
                        </p>
                    </div>
                </div>
            </form>
            
            
        </>
    )
}