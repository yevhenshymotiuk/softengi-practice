import React, {useState, useEffect, useReducer, useRef} from 'react';

export const StarwarsContext = React.createContext();

export const StarwarsProvider = ({children}) => {
    // LIST MANIPULATIONS REDUCER 
    

    // FETCHING DATA
    // ____________________________________________
    useEffect(() => {
        fetchPlanets()
    }, []);

    const [planets, setPlanets] = useState([]);
    const [loading, setLoading] = useState(true);
    const fetchPlanets = async () => {
        // const data = await fetch('https://swapi.co/api/planets/')
        // const planets = await data.json();
        setPlanets([])
        setLoading(false);
    }

    return (
        <StarwarsContext.Provider value={{
            planets, 
            loading, 
            }}>
            {children}
        </StarwarsContext.Provider>
    )
}