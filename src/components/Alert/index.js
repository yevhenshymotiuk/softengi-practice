import React from 'react';
import './alert.css';


export const Alert = () => {
    return (
        <div className="alert-container">
            <div class="alert alert-danger" role="alert">
                API's error!
            </div>
        </div>
    )
}