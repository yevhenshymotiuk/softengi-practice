import React from 'react';
import "./side.css";
import {NavLink, withRouter} from 'react-router-dom';
import fire from '../../utils/fire.js'

const Sidebar = (props) => {
    const {location} = props;
    if (location.pathname.match("/login") || location.pathname.match("/signup")) {
        return null;
    }

    const menu = [
        {link: '/main', style: 'fa fa-home fa-3x', text: 'Home'},
        {link: '/main/starwars', style: 'fa fa-star fa-3x', text: 'Starwars'},
        {link: '/main/dogs', style: 'fa fa-paw fa-3x', text: 'Dogs'},
    ]

    return (
        <div className="sidebar">
            <ul className="sidebar-nav">
                <li className="menu">
                    <i className="fa fa-arrow-circle-o-right fa-3x burger" aria-hidden="true" />
                </li>
                {menu.map((menu, i) => (
                    <li className="side-item" key={i}>
                        <NavLink to={menu.link} className="side-link" exact activeClassName="active-link">
                            <i className={menu.style} aria-hidden="true" />
                            <span className="link-text">{menu.text}</span>
                        </NavLink>
                    </li>
                ))}
                <li className="side-item">
                    <NavLink to="/login" className="side-link" onClick={() => fire.auth().signOut()}>
                        <i className="fa fa-sign-in fa-3x" aria-hidden="true" />
                        <span className="link-text">Log Out</span>
                    </NavLink>
                </li> 

            </ul>  
        </div>
    )}
export default withRouter(Sidebar);
